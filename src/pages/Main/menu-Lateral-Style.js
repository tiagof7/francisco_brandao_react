import styled, { keyframes } from 'styled-components'

const menuHeight = keyframes`
    from {
        height: 70vh;
    }
    to {
        height: 90vh;
    }
`;

export const MenuLateral = styled.nav`
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(131, 19, 19, 0.7);
    min-height: 100vh;
    padding: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 999;
    width: 300px;

    ul {
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        list-style: none;

        li {
            padding: 20px;
            a {
                width: max-content;
                color: #fff;
                position: relative;
                display: flex;
                margin: 0 auto;
                text-transform: uppercase;
                font-size: 1.375em;
                font-family: 'Source Sans Pro';
                cursor: pointer;
                transition: 0.3s all ease-in-out;
                &:not([href]) {
                    color: #fff !important;
                }
                &:before {
                    content: '';
                    width: 0;
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    display: flex;
                    margin: 0 auto;
                    transition: 0.3s all ease-in-out;
                }
                &:hover, &.active {
                    text-decoration: none;
                    color: #fff;
                    &:before {
                        width: 100%;
                        height: 2px;
                        background-color: #fff;
                        z-index: 2;
                        transition: 0.3s all ease-in-out;
                    }
                }
                &.title {
                    font-family: 'GeosansLight';
                    font-size: 2.750em;
                    text-align: center;
                    line-height: 1;
                    opacity: 1;
                    margin-bottom: 2rem;
                    /* overflow: hidden; */
                    height: max-content;
                    &:before {
                        content: none;
                    }
                    &.active {
                        /* height: 0; */
                        opacity: 0;
                        transition: 0.5s all ease-in-out;
                    }
                }
            }
            &:not(:first-child) {
                a {
                    margin: 0 auto;
                    transition: 0.5s all ease-in-out;
                }
            }
        }
        &.active {
            li {
                &:first-child {
                    a {
                        margin-bottom: -100%;
                        transition: 0.5s all ease-in-out;
                    }
                }
                a {
                    margin: 15px auto;
                    transition: 0.5s all ease-in-out;
                }
            }
        }
    }
`;
