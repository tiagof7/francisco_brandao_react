import React, { Component } from 'react';
import { FiChevronDown } from "react-icons/fi";
import { Link, animateScroll as scroll } from "react-scroll";
import VizSensor from 'react-visibility-sensor';

import api from '../../services/api'

import ImageHome from '../../images/home-2.png';
import ImageHome2 from '../../images/home-3.png';
import ImageHome3 from '../../images/home-4.png';
import ImageSection2 from '../../images/biografia.png';
import { MenuLateral } from './menu-Lateral-Style';
import { CarouselHome, Dots, Continue } from './styles';

export default class Main extends Component {
    state = {
        api: api,
        activeIndex: 0,
        activeNav: false,
        nextSection: 2,
    };

    constructor(props) {
        super(props);
        this.titleRef = React.createRef();
    }

    changeSlide = e => {
        this.setState({ activeIndex: e })
    }
    changeNavItem = e => {
        console.log(e)
    }

    componentDidMount() {
        // console.log(this.titleRef.current.state.active)
    }

    componentDidUpdate(_, prevState) {

    }

    render() {
        return (
            <>
                <MenuLateral>
                    <ul className={ this.state.activeNav === false ? 'active' : ''}>
                        <li>
                            <Link
                                activeClass="active"
                                to="section-1"
                                spy={true}
                                smooth={true}
                                duration= {500}
                                className="title"
                                ref={this.titleRef}
                            >
                                Chico <br/> Brandão
                            </Link>
                        </li>
                        <li>
                            <Link
                                activeClass="active"
                                to="section-2"
                                spy={true}
                                smooth={true}
                                duration= {500}
                            >
                                História
                            </Link>
                        </li>
                        <li>
                            <a href="/empreendedorismo">
                                Empreendedorismo
                            </a>
                        </li>
                        <li>
                            <a href="/hipismo">
                                Hipismo
                            </a>
                        </li>
                        <li>
                            <a href="/colecao-de-arte">
                                Coleção de arte
                            </a>
                        </li>
                        <li>
                            <a href="/meio-ambiente">
                                Meio ambiente
                            </a>
                        </li>
                    </ul>
                </MenuLateral>

                <section id="section-1">

                    <CarouselHome className={ this.state.activeIndex === 0 ? 'active' : ''}>
                        <img src={ImageHome} alt=""/>
                        <p>Histórias de uma vida</p>
                        <h1>
                            <p className={ this.state.activeIndex === 0 ? 'active' : ''}>
                                Francisco </p> Brandão
                        </h1>
                        <span>
                            Conheça a trajetória de Francisco Soares Brandão,
                            sócio-fundador da FSB, maior empresa de comunicação
                            corporativa da América Latina.
                        </span>
                    </CarouselHome>
                    <CarouselHome className={ this.state.activeIndex === 1 ? 'active' : ''}>
                        <img src={ImageHome2} alt=""/>
                        <p>Histórias de uma vida</p>
                        <h1>
                            <p className={ this.state.activeIndex === 1 ? 'active' : ''}>
                                Chico</p> Brandão</h1>
                        <span>
                            Conheça a trajetória de Francisco Soares Brandão,
                            sócio-fundador da FSB, maior empresa de comunicação
                            corporativa da América Latina.
                        </span>
                    </CarouselHome>
                    <CarouselHome className={ this.state.activeIndex === 2 ? 'active' : ''}>
                        <img src={ImageHome3} alt=""/>
                        <p>Histórias de uma vida</p>
                        <h1><p className={ this.state.activeIndex === 2 ? 'active' : ''}>
                            Chiquinho</p> Brandão</h1>
                        <span>
                            Conheça a trajetória de Francisco Soares Brandão,
                            sócio-fundador da FSB, maior empresa de comunicação
                            corporativa da América Latina.
                        </span>
                    </CarouselHome>

                    <Dots>
                        <div
                            onClick={this.changeSlide.bind(null, 0)}
                            className={ this.state.activeIndex === 0 ? 'active' : ''}
                        ></div>
                        <div
                            onClick={this.changeSlide.bind(null, 1)}
                            className={ this.state.activeIndex === 1 ? 'active' : ''}
                        ></div>
                        <div
                            onClick={this.changeSlide.bind(null, 2)}
                            className={ this.state.activeIndex === 2 ? 'active' : ''}
                        ></div>
                    </Dots>

                </section>

                <VizSensor
                    onChange={(isVisible) => {
                        this.setState({ activeNav: isVisible })
                    }}
                >
                    <section id="section-2">
                        <img src={ImageSection2} alt="" />
                    </section>
                </VizSensor>

                <Link
                    className="linkContinue"
                    to="section-2"
                    spy={true}
                    smooth={true}
                    duration= {500}
                    >
                    SEGUE
                    <FiChevronDown />
                </Link>
            </>
        )
    }
}
