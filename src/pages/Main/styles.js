import styled, { keyframes } from 'styled-components'

const widthTitle = keyframes`
    from {
        width: 0;
    }

    to {
        width: 100%;
    }
`;
const opacityBanner = keyframes`
    from {
        opacity: 0.5;
    }
    to {
        opacity: 1;
    }
`;

export const CarouselHome = styled.div`
    display: none;
    flex-direction: column;
    justify-content: center;
    height: 100vh;
    margin-left: 320px;
    font-family: 'Source Sans Pro';
    color: #fff;
    transition: 0.3s all ease-in-out;

    img {
        width: 100%;
        height: 100vh;
        object-fit: cover;
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
    }

    p {
        font-weight: 200;
        font-size: 1.563em;
        text-transform: uppercase;
        position: relative;
        width: max-content;

        &:before {
            content: '';
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            display: flex;
            margin: 0 auto;
            height: 1px;
            width: 100%;
            background-color: #fff;
        }
    }

    h1 {
        text-transform: uppercase;
        font-size: 3.938em;
        font-weight: normal;
        display: flex;
        flex-direction: row;
        width: max-content;
        font-family: 'GeosansLight' !important;
        p {
            margin-right: 20px;
            overflow: hidden;
        }
    }

    span {
        width: 50%;
        font-size: 1.563em;
        font-weight: 300;
        font-style: italic;
    }

    &.active {
        display: flex;

        img {
            animation: ${opacityBanner} 1s linear 1;
        }

        h1 {
            p {
                font-size: 1em;
                &.active {
                    animation: ${widthTitle} 1s linear 1;
                }
                &:before {
                    content: none;
                }
            }
        }
    }
`;

export const Dots = styled.div`
    position: absolute;
    z-index: 4;
    display: flex;
    bottom: 6rem;
    left: 320px;
    div {
        width: 16px;
        height: 16px;
        z-index: 5;
        border: 1px solid #fff;
        border-radius: 50%;
        margin: 5px;
        cursor: pointer;

        &.active {
            background-color: #fff;
        }
    }
`;
