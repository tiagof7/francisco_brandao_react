import React from 'react';

// IMPORTANDO ROTAS
import Routes from './routes';

// IMPORTANDO CSS
import 'bootstrap/dist/css/bootstrap.min.css'; // BOOTSTRAP
import GlobalStyle from './styles/global';

function App() {
    return (
        <>
            <Routes />
            <GlobalStyle />
        </>
    );
}

export default App;
