import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
    }
    html, body, #root {
        min-height: 100vh;
    }

    body {
        -webkit-font-smoothing: antialiased !important;
    }

    section {
        position: relative;
    }

    #section-2 {
        padding-bottom: 20vh;
        img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            object-fit: cover;
        }
    }

    .linkContinue {
        position: fixed;
        right: 6rem;
        top: 0;
        bottom: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: auto 0;
        z-index: 5;
        color: #fff !important;
        opacity: 0.4;
        font-size: 2.188em;
        font-family: 'Source Sans Pro';
        height: max-content;
        text-decoration: none !important;
        cursor: pointer;
    }

`;
