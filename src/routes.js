import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Main from './pages/Main'
import Empreendedorismo from './pages/Empreendedorismo'
import Hipismo from './pages/Hipismo'
import Colecao_de_arte from './pages/Colecao-de-arte'
import Meio_ambiente from './pages/Meio-ambiente'

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Main} />
                <Route path="/empreendedorismo" exact component={Empreendedorismo} />
                <Route path="/hipismo" exact component={Hipismo} />
                <Route path="/colecao-de-arte" exact component={Colecao_de_arte} />
                <Route path="/meio-ambiente" exact component={Meio_ambiente} />
            </Switch>
        </BrowserRouter>
    )
}
